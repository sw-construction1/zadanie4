package com.example.proj;

import javax.ejb.*;
import javax.persistence.*;
import java.util.List;

@Stateless
public class EJB_Bean {

    @PersistenceContext(unitName = "myPersistenceUnit")
    private EntityManager em;

    public List<Goods> getAllEntities() {
        TypedQuery<Goods> query = (TypedQuery<Goods>) em.createQuery("SELECT p FROM products p");
        return query.getResultList();
    }

    public void addProduct(String name, String description, String link) {
        Goods product = new Goods(name, description, link);
        em.persist(product);
    }

    public void removeProduct(String name) {
        TypedQuery<Goods> query = (TypedQuery<Goods>) em.createQuery("SELECT p FROM Product p WHERE p.name=:name");
        query.setParameter("name", name);
        List<Goods> resultList = query.getResultList();
        for (Goods product : resultList) {
            em.remove(product);
        }
    }
}
